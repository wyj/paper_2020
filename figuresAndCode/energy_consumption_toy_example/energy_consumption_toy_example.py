import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt



MEASUREMENTSPATH = "./"
FIGURESPATH = "./"

presentation = "save" # ["show", "save"]

sns.set_theme(
    context="talk",
    style="whitegrid",
    rc={
        'figure.autolayout': False,  # figure.autolayout is needed to ensure that the axis labels are not cutoff
        'font.weight': 'bold',
        'axes.labelweight': 'bold'
    }
)



toy = pd.read_csv(MEASUREMENTSPATH+"energy_consumption_toy_example.csv", sep=";")


g = sns.catplot(x="Message Frequency [hours]", y="Lifetime [Years]", col="Signal Conditions", row="RAI", hue="Payload Size [bytes]", data=toy, kind="bar", ci=None)


# try to have everything in one plot
# toy["Payload Size [bytes]"] = toy["Payload Size [bytes]"].astype("category")
# toy["RAI"] = toy["RAI"].astype("category")
# toy["Signal Conditions"] = toy["Signal Conditions"].astype("category")
# toy["Message Frequency [hours]"] = toy["Message Frequency [hours]"].astype("category")
# fig, ax = plt.subplots()    
# ax.clear()
# fig.set_size_inches(10, 5)

# sns.scatterplot(
#     data=toy,
#     x="Message Frequency [hours]",
#     y="Lifetime [Years]",
#     size="Payload Size [bytes]",
#     style="Signal Conditions",
#     hue="RAI",
#     #dashes="RAI",
#     #markers=True,
#     #dashes=True,
#     ax=ax
# )



if presentation == "show":
    plt.show()
else:
    g.savefig(FIGURESPATH + "energy_consumption_toy_example.png", bbox_inches="tight")
