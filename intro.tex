\section{Introduction}
\label{introduction}

\iffalse
The number of Internet-of-things (IoT) applications, services and devices continue to grow
unabated making it a key use category for existing and future cellular network
offerings. Massive machine type communication (mMTC) is one of the three
service categories targeted by emerging 5G mobile network services and a
recent report suggests that there will be 14.7~billion M2M connections by 2023, which
will account for half of the global connected devices and connections~\cite{cisco_report_2020}. 
\fi
In 3GPP, machine type communications (MTC) protocols, like NB-IoT and LTE-M, primarily focus on the capacity and energy challenges 
associated with large scale IoT deployments~\cite{park2020earn}.
These protocols make use of energy saving mechanisms, like discontinuous reception (DRX)
and power saving mode (PSM), to increase battery lifetime. These mechanisms enable
IoT devices
to consume less energy while communicating, especially while monitoring for downlink
communication when the device is in inactive (or sleep) mode.
These mechanisms are used for all IoT applications, but in practice are better suited
to sensor-based applications like meters and measurement sensors. 
Such applications \textit{initiate} communication towards the network, 
transmitting a Mobile Originated (MO) message,
e.g., based on a new measurement,
and are therefore not impacted by energy saving mechanisms like DRX and PSM. (If needed
the device will simply ``wake up'' the communication sub-system, establish connectivity
with the network and send the data.) The same is, however, not true for
sensor-actuator-based applications, like IoT controlled irrigation systems, 
industrial ``smart'' programmable controller (PLC) systems or bike sharing
services. These applications include a (typically downlink) control
aspect, 
transmitting Mobile Terminated (MT) messages,
e.g., stopping/starting an irrigation device, or unlocking a bike for use,
which adds time sensitivity to the communication.
Specifically, for an IoT device in an inactive energy saving state, there might
be significant delay before it can react to the network's notification of 
impending downlink traffic (i.e., a paging request). 


As a concrete example, which we explore in detail in this paper, consider the
flow of messages involved in a bike sharing service: When not in use by a customer,
a bike will periodically send messages to the bike sharing service to report its
current location. These messages originate from the device and as described above,
energy saving methods do not impact the flow of information, i.e.,
here the IoT device might enter into long energy saving periods.
However, when a customer selects a currently unused bike,
the message flow with regards to the IoT device is effectively ``from the network''.
I.e., a bike sharing app on the user's mobile device will interact with the
(cloud-based) bike sharing service (to pay for the service), after which the bike sharing 
service sends an ``unlock'' command to the IoT device on the bike.
To ensure reasonable response times for a user waiting to use the bike,
this actuator action needs to happen in a timely manner, thus creating
tension with the desire to remain in an inactive state to reserve energy
use. This suggests that, for a particular IoT service, 
\textit{different conditions require different parameter settings} 
to balance the tradeoff between energy savings and response time.
Further, the optimal parameter settings and the
relevant tradeoffs between energy savings and latency, will be \textit{different
for different IoT services}, e.g., equipping dumpsters with sensors as part of a
smart waste management program
%~\cite{iot_smart_city_use_cases} 
will have a radically different tradeoff from a bike sharing service.


Given this context, the key question we are exploring is \textit{whether it
is feasible to \textbf{learn} the dynamics of a specific IoT 
service and use that information to dynamically set the parameters of the
energy saving mechanisms in IoT protocols so as to meet the specific
tradeoffs needed by the service.}

To that end we present our work on \Name{}, a service-specific adaptive
data-driven radio resource control architecture for IoT. \Name{} uses data
from network operators and their partners, and uses a network-side machine
learning (ML) based approach to dynamically adapt the radio resource control
(RRC) parameter settings of MTC protocols. 

We specifically focus on dynamically adapting parameter settings associated
with energy saving mechanisms, i.e., DRX and PSM, in NB-IoT and LTE-M. 
To allow service specific needs to be considered, \Name{} makes
use of data from the mobile network operator \textit{and} data associated with
the particular service provider or other relevant third party data
(e.g., from the organization operating the
waste management or bike sharing service, or data concerning weather conditions).
This unique collaboration
capability allows \Name{} to adapt, optimize and tune the energy/latency tradeoff
according to the specific (customer) service needs. 
We use a deep deterministic policy gradient (DDPG) algorithm~\cite{lillicrap2015continuous}, deployed ``within''
the mobile network, to adapt RRC parameter settings to the specific IoT
service using features extracted by a neural network predictor. 
\Name{} assumes and exploits emerging open and programmable (software defined)
network architectures in both the radio access network
(RAN)~\cite{oran,flexran} and the mobile core network~\cite{onap,proteus}
to enable ML-based computation and parameter settings to be introduced into IoT
protocol exchanges. Indeed, to our knowledge, \Name{}
is one of the first end-to-end use cases illustrating the need for and
the utility of, coordinating software-defined actions across both the
RAN and the mobile core network. We make the following specific contributions:


\iffalse
We validate and evaluate our approach
by implementing \Name{} in the ns3 simulation environment. We
evaluate the feasibility of our ML algorithms to adapt and optimize
RRC parameter settings and specifically to provide a tradeoff between 
energy savings and responsiveness. For our evaluations, we make use of
data from a shared mobility service from the Transportation Department 
in Austin Texas. 
\fi

%\textit{Maybe still need to cover: (i) Motivate ``in-network'' approach compared
%client based. (ii) Argue why we need these specific ML approaches.
%But maybe these are covered elsewhere already?}



\begin{itemize}
\item As an RRC parameters configuration approach for real-life IoT applications, 
\Name{} takes both downlink and uplink into consideration. 
The challenge lies in the following three aspects.
First, there is a tradeoff between power consumption and delay for downlink traffic.
Second, communication protocols handle uplink and downlink differently, leading to the fact that the uplink's arrival may impact the downlink's delay.
Third, some real-life applications' traffic does not follow a specific distribution or combinations of several distributions. 
It could be very dynamic but somehow related to the service type.
%These three things add up to make it harder to predict traffic in advance and configure accordingly. 

\item Previous researches predict traffic pattern without any context information other than historical traffic records,
while \Name{} as a network side solution based on the SDRAN framework 
can utilize extra features to provide superior performance with machine learning algorithms.
We compare the traffic pattern's prediction accuracy of \Name{}'s Neural Network predictor 
with other time-series-based algorithms, and the results indicate \Name's superiority.

\item To illustrate the feasibility, we implement our algorithms in the ns3 simulation environment 
and evaluate it with a bike-sharing data from Austin Transportation Department.
In addition, since the RRC parameter’s updating procedure decides when action occurs affecting reward calculation in the reinforcement learning (RL), 
we compare the performances of different event-triggered TAU schemes. 
\end{itemize}


%We present the \Name{} architecture which leverages emerging
%software-defined RAN and core network functionality to allow for
%the dynamic adaption of RAN RRC
%parameters based on machine learning algorithms.
%\item \Name{} makes use of data from both network operators and their
%IoT service provider customers, which offers a unique cooperation
%capability, allowing for service specific optimization and tuning 
%of RRC parameters according to the unique tradeoffs required by the IoT
%service.




The remainder of the paper is organized as follows.
Section~\ref{related} revises related work;
Section~\ref{back} provides background information;
Section~\ref{arch} introduces the architecture of \Name{};
Section~\ref{approach} presents the learning approach of \Name{} through 
a bike sharing use case study;
and section~\ref{Evaluation}  evaluates the performance with the use case.
Finally, Section~\ref{conclusion} concludes the paper.


\iffalse
As a kind of carrier of data, mobile communication has proliferated to keep pace with the demands for the dramatic rise of data traffic. The market is expanding fast; new services are emerging, which drives mobile communication technologies. Even though a higher data-rate is an eternal goal in every generation of mobile communication systems, it is not the only purpose in this new generation. 4G extend and 5G aim at offering diversified services for various service demands, including low data-rates, massive connectivity, ultra-reliability, and low latency. Customers expect user-friendly, easy-to-deploy tools to configure mobile networks based on their service requirements, improve their service quality, and reduce operating costs\cite{Make5G}.

The Internet of Things, which enables seamless communication among massive end devices, is one of the new services mentioned above.  Many Low-power wide-area technologies have emerged to serve hundreds and thousands of heterogeneous smart IoT devices with relatively lower data rates, longer battery life, and broader coverage area requirements. In 3GPP, Machine Type Communications (mMTC) protocols like NB-IoT and LTE-M are to solve the capacity and energy challenges that prevent typical mobile network protocols from broadly using in massive device scenario~\cite{park2020earn}. 

Discontinuous Reception (DRX) and  Power Saving Mode (PSM) are two mechanisms designed for keeping end devices energy efficient by reducing UEs' listening for downlink control information when there is no data transmission. The mechanisms mitigate and ameliorate a series of problems like battery lifetime, manual battery replacement. Though intended to turn the LTE protocol into one suitable for IoT applications that require less energy consumption, their practical usages can only fulfill sensor-based IoT applications with low latency tolerance. Turning off the radio saves energy while leads to longer latency for inactive UEs to receive paging messages. Network configuration strategies and approaches are the keys to solve the problem. "Dummy" end devices will not be smart enough to arrange lower layer network configurations, and application developers, though, can give instructive and reasonable configurations, the real traffic of an application with high flexibility and freedom is usually beyond their understanding. Only adequate knowledge of the traffic and network situation can lead to a strategy balancing the tradeoff between latency and energy. Many eNBs now support following the suggested DRX and PSM parameter settings from the UEs' sides. However, it is neither promising that an optimal setting would be put forward nor realistic to follow UEs' configuration suggestions. An improper configuration may cause massive end devices access to the network and result in the overload of radio access networks (RANs) and paging storm as the result of limited paging capacity~\cite{10.1145/3395351.3399347}. For NB-IoT that covers one RB, the paging capacity would be less. Sensor devices only report the measurements and do not need paging, which may reduce the paging blocking. Nevertheless, it is still an issue for actuator devices. More collaborations between network operators and partner companies are needed to fulfill various services' demands and reasonably schedule network parameters. 
\fi

\iffalse
Other than focusing on upgrading networks with higher speed only, 4G extend and 5G aim at offering diversified services for various service demands, including low data-rates, massive connectivity, ultra-reliability, and low latency. The Internet of Things, which enables seamless communication among massive end devices, is one of the new services. Many Low-power wide-area technologies have emerged to serve hundreds and thousands of heterogeneous smart IoT devices with relatively lower data rates, longer battery life, and broader coverage area requirements. In 3GPP, Machine Type Communications (mMTC) protocols like NB-IoT and LTE-M mainly tackle the capacity and energy challenges that prevent typical mobile network protocols from broadly using in massive device scenario~\cite{park2020earn}. 

Discontinuous Reception (DRX) and Power Saving Mode (PSM) are two mechanisms for keeping end devices energy efficient by consuming less on the downlink monitoring. They mitigate and ameliorate a series of problems like battery lifetime, manual battery replacement. Though intended to turn the LTE protocol into one suitable for all IoT applications, the mechanisms' practical usages are sensor-based applications only. Turning off the radio saves energy while may lead to paging blocks for inactive UEs. Network configuration strategies and approaches are the keys to solve the problem.

A key design question for RRC parameter settings is where to place the functionality of calculating the optimal parameter set under limited network resources. On the one hand, " Dummy" end devices are smart enough to arrange lower layer network configurations. The battery supply cannot support any extra data processing power. Though application developers can give instructive and reasonable configurations, the network's situation is beyond their control. On the other hand, it is neither promising that an optimal setting would be put forward nor realistic to follow UEs' configuration suggestions since eNodeBs also expect efficient parameter settings for relieving the CPU processing limitation. An improper configuration may cause massive end devices access to the network and result in the overload of radio access networks (RANs). More collaborations between network operators and partner companies are needed to fulfill various services' demands and reasonably schedule network parameters. 

Without cooperating with network operators, companies cannot ensure their service quality. There are only limited options for general purposes offered by network operators. Although network operators are not ready for these emerging services, techniques are developing fast. Network slicing has appeared in many high-level conceptual papers for several years, providing more straightforward personalized solutions for business partners and meeting end-users' SLAs. A set of logical networks serve different business purposes on top of a shared infrastructure to fulfill service differentiation. Now SD-RAN platforms like OpenRAN, O-RAN, and FlexRAN provide tools to enable applications to control and manage RAN within network operators' frame. It inspires us to develop RRC parameter reconfiguration solutions using these platforms' tools for different IoT scenarios. Compared to letting IoT application developers design the parameters without enough information on their users' behavior, these SD-RAN platforms' agents can monitor data flows; controller masters can analyze each UE's traffic pattern and give better predictions and parameter settings.


%\begin{figure}[htp] 
%    	\centering
%    	\includegraphics[width=6cm,height=6cm]{figs/intro.png}
%	\caption[]{not ready}
%\label{intro}
%\end{figure}

Traffic patterns of an application can be extracted by monitoring the traffic. IoT applications' traffic characteristics are most regular, and even those irregular traffic would follow some objective laws. Statistical and artificial intelligence-based approaches can address the forecast for predictable traffic patterns. Fig. \ref{intro} shows the difference of traffic patterns inter-application and intra-applications, which makes it evident that the performance of DRX and PSM parameter configuration substantially varies with the traffic load and RAR procedure timing. More importantly, the underlying model capturing this behavior is highly non-linear and far from trivial. Thus we resort to reinforcement learning methods that adapt to the actual users' behavior, traffic loads. Our contributions are as follows:

\begin{trivlist}

  \item We give a detailed framework on leveraging SD-RAN to adapt RRC parameters with machine learning methods on the network side, which has never been discussed before.
  
  \item The framework offers an interface for the partner companies to input extra features and data to improve the machine learning's outcomes.
  
  \item  Our framework also supports partner companies with a tunable QoS level in case of the new demands as a result of any application functions updates.
  
  \item  We conduct experiments with real bike-sharing application data to show how bike-share companies can cooperate with network providers on tuning the network configurations. 
  
\end{trivlist}

(To be modified)The remainder of the paper is organized as follows. Section 2 provides background information about the RRC state machine and some related procedures. The design principles include architecture; the relationship between RRC parameter settings and IoT traffic patterns are described in Section 3. IoT use cases that require both energy-saving and short-latency are given in Section 4 to indicate such demand is not for a particular case. A detailed bike-share case is analyzed later to discuss how network parameters can improve the service with lower energy costs. Section 5 revises the ML-based configuration strategy and its performance applying to the bike-share case. Section 6 concludes the paper. 
\fi

\iffalse
\textbf{Assumption:} This paper analyses the LPWA technologies standardized by 3GPP up to Release 13, focusing on the enhancements introduced in Release 13 for better support of M2M/IoT services
\fi
