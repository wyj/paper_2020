\section{\Name{} Architecture}
\label{arch}
%In this section, we present an overview of \Name{}, the design intention, performance metrics and overall workflows.


\subsection{Design Considerations}

A key factor driving the \Name{} design is the observation that
with IoT services there is a unique symbiotic relationship between
the IoT service provider and the mobile network provider.
First, the IoT service provider is the only party that really
understands the unique requirements and tradeoffs associated
with the IoT service it provides. On the other hand, the network
provider controls the mechanisms through which these requirements
and tradeoffs might be realized. Further, the network provider
has responsibility for the overall stability and performance
of the network for the benefit of all its users. 

These considerations lead to a unique approach
in the \Name{} design whereby IoT service providers and the
network operator \textit{cooperate} to satisfy their respective
interests. Specifically, \Name{} makes use of
data from the IoT provider, as well as an indication of the
tradeoffs important to the IoT provider, and combines that
with network derived data to determine service
specific parameters for the RRC energy saving mechanisms. In turn
the network provider interacts with the IoT service provider 
to enable the parameter value changes to be realized within the
constraints of existing protocol mechanisms. This approach 
balances the needs of both parties. I.e., the IoT provider
benefits from cooperating because its service needs are met, while
the network operator provides a better service while maintaining
control of network mechanisms.

The \Name{} approach has a number of benefits compared
to ``network provider only'', or  ``IoT provider only'' designs.
First, while a network provider only design could take 
network data into account to adapt RRC parameters, such an
approach would not benefit from the IoT data or cooperation
from the IoT provider to enact the changes. Further,
performing the machine learning aspect ``in the network'' where
data characteristics can be readily derived from traffic and
where cloud and edge computing facilities are readily available,
provides for a better design tradeoff compared with attempting
to do machine learning on IoT devices directly, or using relatively
scarce network capacity to convey data from IoT devices to a
centralized point for processing. Also, while RRC parameter
changes could be requested by the IoT device, (e.g., as part of
an IoT provider only solution), such requests by necessity cannot take
current network conditions into account. In contrast,
\Name{}'s approach allows the network to make changes within
paging capacity constraints~\cite{6214167} and to protect the
network against potential overload 
conditions~\cite{bassil2013effects}.

\subsection{\Name{} Overview}

\begin{figure}[htbp]
\vspace*{-0.2cm}
\centerline{\includegraphics[width=0.8\columnwidth]{figs/overview.pdf}}
\vspace*{-0.2cm}
\caption{\Name{} Architecture}
\vspace*{-0.2cm}
\label{overview}
\end{figure}

An overview of the \Name{} architecture is depicted in Fig.~\ref{overview}.
The figure shows the conventional relationship between a mobile provider
operating a cellular infrastructure, consisting of basestations (eNodeBs) and
a core network (EPC), and providing services to an IoT service provider involving
IoT devices (UEs) and an internet/cloud based IoT application server.
As shown in the figure, \Name{} assumes software-defined access,
via a mobile network controller, to both the radio access network
(SD-RAN)~\cite{oran,flexran} and the mobile core network~\cite{onap,proteus}.  
This allows mobile network control applications, like the \Name{} Learning
and Control Application, to obtain data from the mobile network and apply
control actions to influence the behavior of the network and its associated UEs.
Note that we emphasize the fact that these interactions
involve both the RAN and the core network and indeed coordinated interaction
across both enables approaches such as \Name{}. Specifically, for \Name{},
we make use of traffic history obtained from the RAN, while setting parameters
associated with IoT energy saving mechanisms via the core network.
The figure also depicts the explicit interaction between the \Name{}
learning and control application and the IoT application server. This interaction
allows \Name{} to use service specific data from the IoT service provider
as part of its learning process and specifically enables IoT service providers
to provide input to the process based on their service specific tradeoffs.
Finally, as shown, \Name{} can also make use of other third party data sources.
Our insight here is that different IoT services will depend on different
``external'' factors. For example, irrigation systems adjust the watering
frequency based on weather conditions, while the usage of shared mobility services
will fluctuate  based on holidays, rush hour conditions, the availability of
public transportation services, local events and weather conditions.



\iffalse
Fig. \ref{overview} provides a high-level schematic of the \Name{} framework, which is made up of two main components: a data communication module and a network solution module. The data communication module follows SD-RAN architectures to monitor and adjust the network for data transmission among UEs, eNodeBs, and core networks with agents work in concert with a master controller. The communication between the master controller and the agents is bidirectional. In one direction, the agents send the application's traffic-related information (see Section \ref{d1}) to the master controller, while in the other direction, the master delegates control commands of the parameter settings to the agents. The core of the network solution module is a "Learning Box," learning from the input and environment feedback to select actions from a given actions space. It facilitates communication between network operators and their IoT customers by allowing knowledge exchanges between them. The network provides parameter options alleviating traffic pressure. Customers design their service-specific traffic features (see Section \ref{d2}), update features when their application functions change, and upload corresponding data to get better strategies without concern about the network's poor performance due to preempting resources with other applications, leaving aside the possible data security risk and trade secret disclosure.
\fi


\iffalse
\begin{figure}[htbp]
\centerline{\includegraphics[width=9cm]{figs/overview.png}}
\caption{High-level schematic of \Name{}}
\label{overview}
\end{figure}
\fi



\subsection{\Name{} Workflow}
\label{sec:workflow}

\begin{figure}[htbp]
\vspace*{-0.2cm}
\centerline{\includegraphics[width=0.7\columnwidth]{figs/flow.pdf}}
\caption{\Name{} information/message flow}
\vspace*{-0.2cm}
\label{idea}
\end{figure}

Fig.~\ref{idea} depicts the \Name{} information and message flow
within the context of the \Name{} architecture shown in Fig.~\ref{overview}.
(For simplicity we do not show the Mobile Network Controller in Fig.~\ref{idea},
but all communication between the \Name{} application and the network elements
(eNB and MME) takes place via the controller.)
First the \Name{} learning and control application receives data from
the RAN (eNB), the core network (MME),
the IoT application Server and other third party data sources. (Step~(0).)
This data is used in the \Name{} ML algorithms, ultimately leading to a 
change parameter decision. Once a change parameter decision has been made the
new parameter value is communicated to the MME (step~(1)), to be used in subsequent
TAU requests. With the network ``primed''  in this manner the question
is how to trigger a TAU request from the UE so that the parameter setting
can be communicated to it. With reference to Section~\ref{sec:para_set},
\Name{} can make use of two options, namely on-demand TA updating
and periodic updating. Fig.~\ref{idea} depicts on-demand updating.
Specifically, the \Name{} application sends a message to the IoT application
server, requesting that a TAU trigger request be sent to a UE (step~(2)).
The IoT application server uses its application level communication
mechanisms to send a TAU trigger request to the UE in question (step~(3)).
When the UE receives the message it responds by performing
a TAU request (step~(4)), to which the MME in turn will respond with a
TAU accept message containing the updated parameter settings.

Note that, if the UE is in idle mode when the IoT server sends the
application level message (in step~(3)), the RAN might have to
go through a paging procedure to notify the UE of pending downlink
traffic. The key point though is that with this on-demand
TA updating approach the UE will issue the necessary TAU request
which allows the RRC parameters to be updated. We assume this
mechanism in our evaluation of \Name{}.\Name{} can also make use of periodic updating
to update RRC paremeter settings. In that case, once the MME is
primed with the updated parameter setting, \Name{} will simply wait
for the UE in question to perform a periodic updating request. 
With reference to Fig.~\ref{idea}, the flow would be the same, but without 
steps~(2) and~(3). This approach might therefore lead to
some delay before new parameter settings take effect, but might
be preferred in some scenarios. 
In section ~\ref{Evaluation}, We compared the performance of the two TAUs.



\iffalse
As shown in Figure \ref{idea}, the whole architecture of \Name{} follows the loop; an SD-RAN platform collects the data needed for the algorithm on an edge cloud. The algorithm learns traffic patterns with extra custom attributes, chooses the optimal RRC parameter settings, and returns the settings to the master. The master delivers the selected parameters back to the corresponding part of the network. To complete the cycle, we have to determine what to collect with SD-RAN, what to expect from IoT customers, and how to execute the assigned parameter settings.
\fi


%\begin{figure}[htbp]
%\centerline{\includegraphics[width=9cm]{figs/idea.png}}
%\caption{The architecture and workflows of \Name{}}
%\label{idea}
%\end{figure}

\iffalse
\subsection{Design Considerations}



\subsubsection{Why a network side solution}  Any elaborate parameters design that application developers make will never catch up with change. This is especially true speaking of paging in LTE. MME delivers paging requests to all the eNodeBs in a Tracking Area (TA). A malicious attack, a power cutoff, or an overload in a cell leaves unresponsive UEs in the MME's paging queue for a longer time, which derives inefficient paging capacity utilization in other cells that belong to the same TA~\cite{6214167}. In this case, RRC parameters should follow up the real-time network situations, which is beyond an end-point solution's calculation and power ability. Rather than developing tools for parameter settings themselves, customers need advice from the network and service-specific solutions ready-to-use.

Besides, current eNodeBs and MMEs' processing power do not match the need for synchronized massive access. Assume one paging occasion in each paging frame, and each occasion's paging list has 16 paging records. The total number of paging messages reaches 1600 paging records per second. Suppose all these paging messages all arrive at UEs and cause massive random access to simultaneously ask for service. Can eNodeBs and MMEs deal with the magnitude? From researches, 75 UEs' service requests occupy 8\% of the CPU utilization per second in an eNodeB~\cite{bassil2013effects} and the processing delay in MMEs surges after a particular threshold to 800ms when the total service requests over 900 per second~\cite{10.1145/2716281.2836104}. This implies the network also has demands on evenly distributed paging messages rather than a burst of them. The timeline should be justly distributed according to the applications' service types.

\subsubsection{Desgin goals of \Name{}} From above mentioned reasons, we conclude the following points for \Name{} to address. 
\begin{itemize}
\item A network side solution that doesn't put extra processing power on end devices.

\item Allow the network to offer applications alternative RRC parameter values based on the actual network situation. A possible future work is to design algorithm for the network allocate the  timeline and processing resources via parameter settings. 

\item Support different applications select the network provided RRC parameters according to their traffic patterns.

\end{itemize}
\fi

\iffalse
\subsection{Performance metrics}
To realize \Name{}'s objectives an ideal learning
algorithm should allow a tradeoff between two metrics, namely, 
energy consumption and paging latency. 
%\textit{XXX: Not sure this is the right place for this... It might better fit in the evaluation section.}

\subsubsection{Energy consumption} 
UE power consumption vary depending on the RRC state (see Figure~\ref{fig-rrc}).
The total amount of energy consumed is the sum up of the UEs' power usage in
a particular state, multiplied by the time spent in each state.  
%The radio use intensity results in the lifts up and down of the power. There is a
%significant power drop comparing the RRC connected with the RRC idle state. 
As a network side solution, \Name{} can not directly measure UEs' energy
consumption. However it is possible to accurately estimate the total energy
used simply based on the time spent in each state.
%with devices whose voltage and average current in different RRC states have been gained already is feasible. 


\subsubsection{Paging latency}
For our purposes, we define paging latency as the time between MME sending
the S1 Paging to indicate the arrival of downstream traffic and receiving the
UE's paging response.
\fi

\iffalse
 In general, Quality of Service (QoS) can partially measure the paging latency. In legacy LTE, QoS is used to decide the bearer type for transmitting a packet as a latency limitation.  One of the QoS parameters called Packet Delay Budget (PDB) defines an upper bound for packet transmissions between the UEs and the Policy and Charging Enforcement Function (PCEF), a module for receiving IP flows in EPC. For the sake of convenience, We narrow the definition down to the range between UEs and the MME.
\fi

\iffalse

\subsubsection{Data to collect with SD-RAN} \label{d1}
For many IoT applications, traffic patterns can be extracted as a time series problem. The general traffic records like traffic direction, the time interval between two traffic, and previous traffic intervals are traffic pattern related data to collect with SD-RAN depending on the customers' demands. In this paper, customers are only allowed to invoke the traffic features provided by the network. An improved interface with a well designed programming language is a potential future work. Aside from these characteristic data, to evaluate the algorithm's performance, the master should provide the energy estimator timings of RRC state transmission and record the paging latency as we defined before. 



\subsubsection{Custom attributes}\label{d2}
Traffic records, as the results, not the causes, cannot explain complicated traffic patterns when other factors also impact the traffic. Irrigation systems adjust the watering frequency based on the weather and climate. Industrial output is fixed through the relationship of supply and demand. The usage of shared bikes fluctuates with the holiday, rush hour, public transportation opening time, local events, total bike amount, and weather in the area. These examples contend the necessity of custom attributes. Thus, \Name{} is designed to read in attribute data from costumers or third-parties as complementary features according to consultations.


\subsubsection{Parameters despatch}
\label{pd}
As introduced in Section \ref{background}, parameters despatch through TAU response messages triggered by TAU requests. When and how frequently a UE should make the TAU request become challenging problems. Following are three possible solutions. We set RRC parameters' IEs with random values to leave MMEs to choose optimal parameter values.

\textbf{Solution 1:}  A periodic TAU's original purpose is to update a UE's TA considering UEs' movement during their PSM. However,  attaching an RRC modification request identity entry(IE) in a periodic TAU request is feasible because checking optional IEs each time receiving a TAU request follows the protocol's standard. The problem lies in that there is no chance to reconfigure the parameters in the extreme cases that MO messages cutting short every PSM and prompt UEs enter the next connected state before triggering any periodic TAU requests.

\textbf{Solution 2:}  "TA updating" refers to specific TAU with which UEs acknowledge the network its special setting needs. This TAU does not have to be after PSM. Setting a periodic "TA updating" request with RRC parameters' IEs avoids the extreme case mentioned in solution 1. However, this solution brings extra TAU request, accept signalings and requires application developers to set a proper periodicity for the TAU. 

\textbf{Solution 3:}  Each data transmission updates the input of the algorithm and generates a new parameter setting. If the setting changes, the application server should be informed of the target UE's IMSI and send a packet with a command to activate a "TA updating" request in the UE within that RRC inactivity time. In this way, the RRC parameter reconfiguration can be complete before the next traffic.
\fi















\iffalse
Power saving parameters are RRC layer timers that control the radio sending and receiving on and off. They can be classified into different groups based on the state where they are in effect. Due to the different state they serve, resetting these parameters are also different. To complete the loop in Fig. \ref{overview}, we need first to understand which parameters to modify can lead to desired performance and how to reach an agreement on both UE and network sides under the original LTE communication framework.
\fi

\iffalse
To reduce UEs' redundant and frequent energy waste when listening to the downlink channel, DRX and PSM are taken into action in protocols for IoT scenarios. The two power-saving mechanisms bring the paging latency problem mentioned above. An algorithm that can find a better tradeoff between Power saving and paging latency is needed to assist IoT applications in collecting sensed data while acting according to the servers' commands. Thus besides paging latency, energy consumption is another metric to evaluate the algorithm.
The total amount of energy consumed depends on UEs' power and the total time they are in use. However, UEs' power consumptions vary with RRC state transmissions. There is a significant power drop comparing the RRC connected with the RRC I-eDRX state. Power lifts up and down because the UE turns the radio listen mode on and off. Though our algorithm can not directly measure UEs' energy consumption, estimate the total energy used with devices whose voltage and average current in different RRC states have been gained already is feasible. 
\fi

\iffalse
Quality of Service (QoS) can partially measure the paging latency. This concept has different meanings across the protocols. In some NB-IoT client app, this QoS is an application layer concept to indicate whether the publisher requires responses after posting anything which is not directly related to paging latency. In legacy LTE, QoS is used to decide the bearer type for transmitting a packet. Once a Policy and Charging Enforcement Function (PCEF) receives the IP flows, Service Data Flow (SDF) packet filters match the corresponding SDF Policies by the service characteristics. Different SDF policies utilize EPS bearers with different Quality of Service (QoS) to maintain the QoS demands from SDF Policies. Here, one of the SDF QoS Parameters called Packet Delay Budget (PDB) defines an upper bound for packet transmissions between the UEs and the PCEF, including the paging latency and data transmission time. QoS is strictly regulated in 3GPP TS 23.203 that clearly stated the different PDBs for different services. However, for the cases that applied DRX, notes in the protocol explain that the QoS level can be relaxed for the first packet in a downlink data or signaling burst to permit battery saving (DRX) techniques but no further explanations about how. It is reasonable because such latency sometimes is ignorable when the interval between each OnDuration in DRX is small enough to meet some latency-tolerant service requirements. In this paper, we also define a QoS parameter that is different from all mentioned above. It is a parameter for customers to tune the RRC parameter value selection algorithm based on the applications' demands. 

One of the QoS parameters called Packet Delay Budget (PDB) defines an upper bound for packet transmissions between the UEs and the Policy and Charging Enforcement Function (PCEF), a module for receiving IP flows in EPC. PDB is the reference used as a latency constrain for RRC parameter settings'  optimization solutions. However, 3GPP does not give any further instruction on how to relax the QoS level for DRX applications.
\fi

\iffalse
From figure \ref{paging}, a packet flow arrives at an EPC, which triggers the MME to send a paging message to eNodeBs in the TAC list. The eNodeB connecting to the target UE broadcasts a paging list according to the corresponding paging occasion records. And then, the target UE is supposed to listen to the downlink control channel, catch the paging list, thus launch a random access request for a bearer and receive the packet flow.  For some reason, the packet flow may not arrive at the UE on time as expected, and we call this paging latency. The reasons include the limited paging capacity and PSM interval. Not all the paging messages will be delivered in time for IoT scenarios with massive end devices, as we introduce the paging blocking issue in the related work. Figure \ref{paging} depicts how paging blocking happens. What's more, some downlink direction packet flows appear randomly when the target UEs are still in PSM, and thus the flows will not reach the end devices and have to be buffered until UEs wake up. 

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.4]{figs/paginglatency.png}
    \caption[]{Paging blocking}
    \label{paging}
\end{figure}
\fi

\iffalse
\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.25]{figs/SDF&QoS.png}
    \caption[]{SDFs and QoS}
    \label{QoS}
\end{figure}
\fi

\iffalse
\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.25]{figs/power.png}
    \caption[]{Current}
    \label{QoS}
\end{figure}

\subsection{ End-point side solutions}
End-point solutions like a Portable ML-based monitor~\cite{hu2019nb} and TinyLink 2.0~\cite{10.1145/3372224.3380890} provide IoT application developers more domain-specific knowledge and thus improve products' performance. The former monitors RRC parameter settings, and the latter analyzes traffic characteristics. Integrating the two turns into an end-point solution that end devices send the RRC parameters to the cloud and get the optimal parameter settings back. The solution is feasible while it ignores the fact that UEs cannot afford extra power consumptions caused by all kinds of calculations and monitoring. IoT end devices are supposed to be battery powered with simple functionality. Not only that, the utility ratio of the connected MME's paging capacity and the paging priority are transparent for UEs. It is still possible for a UE to receive a paging message during the radio's off duration led by its "shortsighted" and "aggressive" parameter settings. Thus, in this paper, we present network side solutions for various IoT devices to modify the RRC parameters.
\fi

\iffalse
As shown in Figure \ref{idea}, a master controller collects data from agents in eNBs, and MMEs and pass forward to the algorithm on an edge cloud. The algorithm learns the traffic with extra necessary information from the internet and application developers, chooses the optimal RRC parameter settings, and returns the settings to corresponding agents to execute reconfiguration.
\fi
